# RE-Energym


## Team Members

Juan de Luna s1118614

Varga Stefan s1114115

Gavajuc Alexandru s1117680

Martina Tébar s1115571

## Requirements for running
1. Follow intallation https://docs.expo.dev/router/installation/
3. Intall in the play store or app store Expo Go (We only tested the app in IOS)
4. Run "npx expo start -c" in the terminal
5. Scan the QR with the mobile phone.

In case the running of the app gives an error we will app a recording of the app in the zip file called "Recording_app"


## Info about structure

All screens of the app are located in the "app" folder, the introduction file is "index", the authentication files, "sign-in" and "sign-up" are in "(auth)" and the main pages after the signing in are in "(tabs)". Apart from that there are more folders that contains subsections of the screens mentioned before.

##Gitlab resources

Gitlab link: https://gitlab.science.ru.nl/vvarga/re-energym
The 'main' branch should work. In case it does not, 'main-db-sasha' is our backup branch.