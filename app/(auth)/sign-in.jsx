import { View, Text, ScrollView,Alert } from 'react-native'
import React, {useState} from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Image } from 'react-native'
import {images } from '../../constants';
import FormField from '../../components/FormField';
import CustomButtom from '../../components/CustomButtom';
import { Link, router } from 'expo-router';
import { signIn, getCurrentUser, getAccount } from '../../lib/appwrite';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useGlobalContext } from '../../context/GlobalProvider';
import {useImageState} from '../../context/ImageStateContext';

const SignIn = () => {

  const { imageUri, setImageUri } = useImageState();
  const { setUser, setIsLogged } = useGlobalContext();
  const { user, isLogged } = useGlobalContext();
    const [form, setForm] = useState({
        email: '',
        password: '',
    })

    const [isSumbitting, setisSumbitting] = useState(false)

    const sumbit = async () => {
      if(!form.email || !form.password){
        Alert.alert('Error', 'Please fill in all fields')
      }
      setisSumbitting(true);
      try {
        await signIn(form.email, form.password)
        //set to global

   

        const result = await getAccount();
        console.log("Fetched user:", result); // Debug log
        setUser(result);
        setIsLogged(true);
        setImageUri(null);
        console.log("User set in context:", result); // Debug log


        router.replace('/home')
      } catch (error) {
        Alert.alert('Error', error.message)
      }finally{
        setisSumbitting(false)
      }
    }

  return (
    <SafeAreaView classNamer = "bg-primary h-full">

      <KeyboardAwareScrollView extraScrollHeight={90} keyboardOpeningTime={0} enableOnAndroid={true}>
        <View className = "w-full justified-center min-h-[83vh] px-4 my-6">
            <Image 
                source={images.logo}
                className = 'w-[230px] h-[300px]'
                style={{marginLeft:60}}
            />
            <Text className = "text-2xl text-black text-semibold mt- font-psemibold"  >
                Log in to Energym
            </Text>
            <FormField 
                title = "Email"
                value = {form.email}
                handleChangeText={(e) => setForm({...form, email: e})}
                otherStyles="mt-6"
                keyboardType = "email-address"
            />

            <FormField
                title = "Password"
                value = {form.password}
                handleChangeText={(e) => setForm({...form, password: e})}
                otherStyles="mt-6"
            />

            <CustomButtom
             title = "Sign In"
             handlePress={sumbit}
             //margin top of seven
             containerStyles="mt-7"
             isLoading={isSumbitting}
             //Flex row was to put the buttom eye in the 
            />
            
            <View className= "justify-center pt-5 flex-row gap-2">
              <Text className= "text-lg text-gray-100 font-pregular">
                 Don't have account?
              </Text>
              <Link href="/sign-up" className="text-lg font-psemibold text-secondary">
              Sign up</Link>
            </View>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  )
}

export default SignIn