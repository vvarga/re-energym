import { View, Text, ScrollView, Alert } from 'react-native'
import React, {useState} from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import FormField from '../../components/FormField';
import CustomButtom from '../../components/CustomButtom';
import { Link, router } from 'expo-router';
import { createUser } from '../../lib/appwrite';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const SignUp = () => {
    const [form, setForm] = useState({
        username: '',
        email: '',
        password: '',
    })

    const [isSumbitting, setisSumbitting] = useState(false)

    const sumbit = async () => {
      if(!form.username || !form.email || !form.password){
        Alert.alert('Error', 'Please fill in all fields')
      }
      setisSumbitting(true);
      try {
        const result=await createUser(form.email, form.password, form.username)
        //set to global
        
        router.replace('/home')
      } catch (error) {
        Alert.alert('Error', error.message)
      }finally{
        setisSumbitting(false)
      }
    }

  return (
    <SafeAreaView classNamer = "bg-primary h-full">
      <KeyboardAwareScrollView extraScrollHeight={90} keyboardOpeningTime={0} enableOnAndroid={true}>
        <View className = "w-full justified-center min-h-[83vh] px-4 my-6">
         
            <Text className = "text-2xl text-black text-semibold mt-4 font-psemibold"  >
                Sign Up to Energym
            </Text>
            <FormField 
                title = "Username "
                value = {form.username}
                handleChangeText={(e) => setForm({...form, username: e})}
                otherStyles="mt-6"
            />
            <FormField 
                title = "Email"
                value = {form.email}
                handleChangeText={(e) => setForm({...form, email: e})}
                otherStyles="mt-6"
                keyboardType = "email-address"
            />

            <FormField
                title = "Password"
                value = {form.password}
                handleChangeText={(e) => setForm({...form, password: e})}
                otherStyles="mt-6"
                
            />

            <CustomButtom
             title = "Create Account"
             handlePress={sumbit}
             //margin top of seven
             containerStyles="mt-7"
             isLoading={isSumbitting}
             //Flex row was to put the buttom eye in the 
            />
            
            
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  )
}

export default SignUp