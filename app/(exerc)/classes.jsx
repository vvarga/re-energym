

import React, { useState } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Dimensions, ScrollView, Image } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { images } from '../../constants';
import { icons } from '../../constants';
import { router } from 'expo-router';



const { width } = Dimensions.get('window');

const classes = () => {
  const [registered, setRegistered] = useState([false, false, false]);

  const handleRegister = (index) => {
    const newRegistered = [...registered];
    newRegistered[index] = !newRegistered[index];
    setRegistered(newRegistered);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
      <TouchableOpacity style={styles.topLeftButton} onPress={() => router.back()}>
        <Image source={icons.leftArrow} style={styles.arrowIcon} />
      </TouchableOpacity>
        <Text style={styles.title}>Classes</Text>
        <View style={styles.registrationContainer}>
          <Text style={{ fontSize: 20, marginTop: 35 }} className="text-lg font-psemibold text-secondary text-center">
            Click to register
          </Text>
          <Image source={icons.check} style={styles.Icon} className="image-center" />
        </View>
       
        <TouchableOpacity
          style={styles.fullWidthShortButton}
          className="mt-0.5 px-4 h-10"
          onPress={() => handleRegister(0)}
        >
          {registered[0] ? (
            <Text style={styles.registeredText} >Registered</Text>
          ) : (
            <Image source={images.spinning} />
          )}
        </TouchableOpacity>
        <TouchableOpacity style={styles.fullWidthShortButton} onPress={() => handleRegister(1)}>
          {registered[1] ? (
            <Text style={styles.registeredText}>Registered</Text>
          ) : (
            <Image source={images.kfit} />
          )}
        </TouchableOpacity>
        <TouchableOpacity style={styles.fullWidthShortButton} onPress={() => handleRegister(2)}>
          {registered[2] ? (
            <Text style={styles.registeredText}>Registered</Text>
          ) : (
            <Image source={images.full} />
          )}
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

const buttonWidth = (width - 70) / 2; // Subtracting padding and margins
const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
  },
 
  fullWidthShortButton: {
    width: width - 30, // Adjust width to match the screen width minus padding
    height: buttonWidth, // Make the button square
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20, // Add horizontal margin to match the button container
    paddingHorizontal: 15,
    borderRadius: 30, // Adjust the border radius as needed
    marginTop: 50, // Add margin top for spacing
  },
  topLeftButton: {
    position: 'absolute',
    top: 5,
    left: 10,
    padding: 15,
    borderRadius: 20,
    zIndex: 1, // Ensure the button is on top
    marginTop:20,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between', // Align buttons with equal spacing between them
    paddingHorizontal: 20, // Add horizontal padding to the button container
    marginTop: 10, // Add margin top for spacing
  
  },
  Icon: {
    width: 35, // Adjust width as needed
    height: 30, // Adjust height as needed
    marginLeft: 8,
    marginTop: 30,
    marginLeft: 20,
    tintColor: 'orange'
  },
  registrationContainer: {
    flexDirection: 'row', // Arrange items horizontally
    alignItems: 'center', // Center items vertically
    justifyContent: 'center', // Center items horizontally
    marginTop: 5,
    color: 'orange'
  },
  registeredText: {
    color: 'orange',
    fontSize: 30,
    fontWeight: 'bold',
  },
  arrowIcon: {
    width: 20,
    height: 15,
    tintColor: 'black', // Adjust the color if necessary
  },
});

export default classes;
