
import { View, Text } from 'react-native'
import { Stack } from 'expo-router'
import { StatusBar } from 'expo-status-bar'

const SocialLayout = () => {
    return (
      <>
          <Stack>
              <Stack.Screen 
                  name = "trophies"
                  options={{headerShown: false}}
              
              />
              <Stack.Screen 
                  name = "challenge"
                  options={{headerShown: false}}
              
              />
          </Stack>
          <StatusBar backgroundColor='#FFFFFF' style='light'/>
      </>
    )
  }
  
  export default SocialLayout