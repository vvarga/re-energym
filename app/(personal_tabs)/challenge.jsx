import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Dimensions, ScrollView, Image } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { images } from '../../constants';
import { icons } from '../../constants';
import { router } from 'expo-router';
import { useGlobalContext } from '../../context/GlobalProvider';

const { width } = Dimensions.get('window');

const Challenges = () => {
  const { setChallenge } = useGlobalContext();

  return (
    <ScrollView style={{ backgroundColor: '#fff' }}>
      <View style={styles.container}>
        <Text style={{ fontSize: 20, marginTop: 35 }} className="text-lg font-psemibold text-secondary text-center">
          Challenge yourself
        </Text>
      </View>
      <TouchableOpacity style={styles.topLeftButton} onPress={() => router.dismiss()}>
        <Image source={icons.leftArrow} style={styles.arrowIcon} />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.fullWidthShortButton}
        onPress={() => { setChallenge(1); router.back(); }}
      >
        <Image source={images.ch1} />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.fullWidthShortButton}
        onPress={() => { setChallenge(2); router.back(); }}
      >
        <Image source={images.ch2} />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.fullWidthShortButton}
        onPress={() => { setChallenge(3); router.back(); }}
      >
        <Image source={images.ch3} />
      </TouchableOpacity>
    </ScrollView>
  );
};

const buttonWidth = (width - 70) / 2; // Subtracting padding and margins
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  topLeftButton: {
    position: 'absolute',
    top: 5,
    left: 10,
    padding: 15,
    borderRadius: 20,
    zIndex: 1, // Ensure the button is on top
    marginTop: 20,
  },
  fullWidthShortButton: {
    width: width - 30, // Adjust width to match the screen width minus padding
    height: buttonWidth, // Make the button square
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20, // Add horizontal margin to match the button container
    paddingHorizontal: 15,
    borderRadius: 30, // Adjust the border radius as needed
    marginTop: 50, // Add margin top for spacing
  },
  arrowIcon: {
    width: 35, // Adjust width as needed
    height: 30, // Adjust height as needed
    tintColor: 'orange',
  },
  topLeftButton: {
    position: 'absolute',
    top: 30,
    left: 10,
    padding: 15,
    borderRadius: 20,
    zIndex: 1, // Ensure the button is on top
    marginTop:20,
  },
  arrowIcon: {
    width: 20,
    height: 15,
    tintColor: 'black', // Adjust the color if necessary
  },
});

export default Challenges;
