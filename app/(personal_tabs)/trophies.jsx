import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Dimensions, ScrollView, Image } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { images } from '../../constants';
import { icons } from '../../constants';
import { router } from 'expo-router';

const { width } = Dimensions.get('window');

const add_friend = () => {
  return (
      <ScrollView  style={{backgroundColor: '#fff'}}>
        <View style={styles.container}>
        <View style={styles.registrationContainer}>
          <Text style={{ fontSize: 20, marginTop: 35 }} className="text-lg font-psemibold text-secondary text-center">
            Awards
          </Text>
        </View>
        <TouchableOpacity style={styles.topLeftButton} onPress={() => router.dismiss()}>
        <Image source={icons.leftArrow} style={styles.arrowIcon} />
      </TouchableOpacity>
      
        <TouchableOpacity
          style={styles.fullWidthShortButton}  
          className="mt-0.5 px-4 h-10"
          
        >
          <Image source={images.trf1} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.fullWidthShortButton}
          
        >
          <Image source={images.trf2} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.fullWidthShortButton}
      
        >
          <Image source={images.trf3} />
        </TouchableOpacity>
        </View>
      </ScrollView>
  );
}; 

const buttonWidth = (width - 70) / 2; // Subtracting padding and margins
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
  },
  topLeftButton: {
    position: 'absolute',
    top: 5,
    left: 10,
    padding: 15,
    borderRadius: 20,
    zIndex: 1, // Ensure the button is on top
    marginTop:20,
  },
  fullWidthShortButton: {
    width: width - 30, // Adjust width to match the screen width minus padding
    height: buttonWidth, // Make the button square
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20, // Add horizontal margin to match the button container
    paddingHorizontal: 15,
    borderRadius: 30, // Adjust the border radius as needed
    marginTop: 50, // Add margin top for spacing
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between', // Align buttons with equal spacing between them
    paddingHorizontal: 20, // Add horizontal padding to the button container
    marginTop: 20, // Add margin top for spacing
  },
  Icon: {
    width: 35, // Adjust width as needed
    height: 30, // Adjust height as needed
    marginLeft: 40,
    marginTop: 4,
    tintColor: 'orange'
  },
  topLeftButton: {
    position: 'absolute',
    top: 30,
    left: 10,
    padding: 15,
    borderRadius: 20,
    zIndex: 1, // Ensure the button is on top
    marginTop:20,
  },
  arrowIcon: {
    width: 20,
    height: 15,
    tintColor: 'black', // Adjust the color if necessary
  },
});
  export default add_friend;
