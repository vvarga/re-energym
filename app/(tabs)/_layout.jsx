import React from 'react';
import { View, Text, Image } from 'react-native';
import { Tabs, router } from 'expo-router';
// import Header from '../Header'; // Import the Header component
import { icons } from '../../constants';
import Header from '../../components/Header';

const TabIcon = ({ icon, color, name, focused }) => {
  let iconSize = { width: 24, height: 24 }; // Default size for icons
  if (name === "Exercise") {
    iconSize = { width: 45, height: 30 };
  }
  if (name === "Home") {
    iconSize = { width: 26, height: 26 };
  }
  if (name === "Personal") {
    iconSize = { width: 34, height: 30 };
  }
  return (
    <View style={{ alignItems: 'center', justifyContent: 'center', gap: 2 }}>
      <Image 
        source={icon}
        resizeMode="contain"
        tintColor={color}
        style={iconSize} // Apply the size style
      />
      <Text style={{ color, fontSize: 12, fontFamily: focused ? 'font-psemibold' : 'font-pregular' }}>
        {name}
      </Text>
    </View>
  );
};

const TabsLayout = () => {
  const handleButton1Press = () => {
    router.push('/notifications')
  };

  const handleButton2Press = () => {
    router.push('/account')
  };

  return (
    <>
      <Header onPressButton1={handleButton1Press} onPressButton2={handleButton2Press} />

      <Tabs
        screenOptions={{
          tabBarShowLabel: false,
          tabBarActiveTintColor: '#FFA001',
          tabBarInactiveTintColor: '#949191',
          tabBarStyle: {
            borderTopWidth: 1,
            borderTopColor: '#E8E8E8',
            height: 95,
          }
        }}
      >
        <Tabs.Screen 
          name="exercise"
          options={{
            title: 'Exercise',
            headerShown: false,
            tabBarIcon: ({ color, focused }) => (
              <TabIcon 
                icon={icons.bookmark}
                color={color}
                name="Exercise"
                focused={focused}
              />
            )
          }}
        />
        <Tabs.Screen 
          name="home"
          options={{
            title: 'Home',
            headerShown: false,
            tabBarIcon: ({ color, focused }) => (
              <TabIcon 
                icon={icons.home}
                color={color}
                name="Home"
                focused={focused}
              />
            )
          }}
        />
        <Tabs.Screen 
          name="personal"
          options={{
            title: 'Personal',
            headerShown: false,
            tabBarIcon: ({ color, focused }) => (
              <TabIcon 
                icon={icons.social}
                color={color}
                name="Personal"
                focused={focused}
              />
            )
          }}
        />
      </Tabs>
    </>
  );
};

export default TabsLayout;
