import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Dimensions, ScrollView, Image, Button, Modal, Alert } from 'react-native';
import { icons } from '../../constants';
import { images } from '../../constants';
import { Camera, CameraView } from 'expo-camera';
import * as Haptics from 'expo-haptics';
import { router } from 'expo-router';
import { getMachineDetails } from '../../lib/appwrite';  // Adjust the import path as needed



const { width } = Dimensions.get('window');

const Exercise = () => {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [showScanner, setShowScanner] = useState(false);
  const [machine, setMachine] = useState(null);


  useEffect(() => {
    const getCameraPermissions = async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === "granted");
    };

    getCameraPermissions();

  }, []);


  useEffect(() => {
    renderMachine();
  }, [machine]);

  
  const renderMachine = () => {
    switch (machine) {
      case 'Treadmill':
        return <Image source={images.treadmill} style={styles.image} />;
      case 'Squat Machine':
        return <Image source={images.squat_m} style={styles.image} />;
      case 'Chest Press Machine':
        return <Image source={images.chest_press_m} style={styles.image} />;
      case 'Leg Extension Machine':
        return <Image source={images.legextension_m} style={styles.image} />;
      case 'Leg Press Machine':
        return <Image source={images.legpress_m} style={styles.image} />;
      case 'Row Machine':
        return <Image source={images.row_m} style={styles.image} />;
      case 'Cable Machine':
        return <Image source={images.cable_m} style={styles.image} />;
      default:
        return <Image source={images.treadmill} style={styles.image} />;
    }
  };
  

  const handleBarCodeScanned = async ({ type, data }) => {
    setScanned(true);
    setShowScanner(false);

    Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success); // Trigger haptic feedback if available
    try {
      console.log(data);
      const machineDetails = await getMachineDetails(data);  // Assuming data contains the machine ID
    
      setMachine(data);
    } catch (error) {
      console.error('Failed to fetch machine details:', error);
      // Optionally show an error message to the user
    }
  };


  
  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <ScrollView style={styles.container}>
      <Modal
        visible={showScanner}
        transparent={false}
        presentationStyle='pageSheet'
        animationType="slide"
        onRequestClose={() => setShowScanner(false)}
      >
        <View style={styles.modalContainer}>
          {showScanner && <CameraView
            onBarcodeScanned={scanned ? undefined : handleBarCodeScanned}

            barcodeScannerSettings={{
              barcodeTypes: ["qr", "pdf417"],
            }}
            style={StyleSheet.absoluteFillObject}
          />}
          <TouchableOpacity style={styles.cameraButton} onPress={() => setShowScanner(false)}>
            <Text style={styles.buttonText}>Close Scanner</Text>
          </TouchableOpacity>
        </View>
      </Modal>

      <Text style={styles.title}>Exercise</Text>
      <TouchableOpacity style={styles.fullWidthButton} onPress={() => { setShowScanner(true), setScanned(false) }}>
        
        {!scanned ? (
          <>
            <Image source={icons.qr_logo} style={styles.plusLogo} />
            <Text style={styles.buttonText}>Scan your device</Text>
          </>
        ) : (
          <>
          <TouchableOpacity style={styles.topLeftContainer}  onPress={() =>{setScanned(false)}}>
          <Image source={icons.logout} style={styles.smallButton} />
          <Text style={styles.smallText}>Disconnect</Text>
        </TouchableOpacity>
            {renderMachine()}
       
            <Text style={styles.buttonText}>Press to scan a new macine</Text>
          </>
        )}
      </TouchableOpacity>

      <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.button} onPress={()=>router.push('/classes')}>
            <Text style={styles.buttonText}>Classes</Text>
          </TouchableOpacity>
       
          <TouchableOpacity style={styles.button} onPress={()=>router.push('/cardio')}>
            <Text style={styles.buttonText}>Workouts</Text> 
          </TouchableOpacity>
        
        </View>
      </ScrollView>
  );
};

const buttonWidth = (width - 60) / 2; // Subtracting padding and margins

const styles = StyleSheet.create({
  topLeftContainer: {
    position: 'absolute',
    top: 10,
    left: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  smallButton: {
    width: 20,
    height: 20,
  
 
  },
  smallText: {
    marginLeft: 5,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  modalContainer: {
    flex: 1,
  },
  fullWidthButton: {
    width: width - 40,
    height: width - 40,
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20,
    borderRadius: 20,
    marginTop: 10,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 4, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 4, //IOS
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginTop: 20,
  },
  plusLogo: {
    width: 100,
    height: 100,
    marginBottom: 30,
    tintColor: 'black',
  },
  image: {
    width: 250,
    height: 250,
    marginBottom: 30,
  },
  cameraButton: {
    position: 'absolute',
    bottom: 100,
    left: (width - buttonWidth) / 2, // Center the button horizontally
    width: buttonWidth,
    height: 60,
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 4, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 4, //IOS
  },
  button: {
    width: buttonWidth,
    height: buttonWidth,
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 4, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 4, //IOS
  },
  buttonText: {
    color: 'black',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default Exercise;
