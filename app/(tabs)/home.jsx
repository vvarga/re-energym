import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Dimensions, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import Animated, { Easing, useSharedValue, useAnimatedProps, withTiming } from 'react-native-reanimated';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useGlobalContext } from '../../context/GlobalProvider';
import { images } from '../../constants';

const { width } = Dimensions.get('window');
const buttonWidth = (width - 60) / 2; // Subtracting padding and margins
const AnimatedPath = Animated.createAnimatedComponent(Path);

const SemiCircularProgress = ({ progress }) => {
  const r = width / 2 - 35; // Radius of the semicircle (adjust for padding/margin)
  const strokeWidth = 30;
  const cx = width / 2; // X center of the semicircle
  const cy = r + strokeWidth / 2; // Y center of the semicircle
  const circumference = Math.PI * r; // Circumference of the semicircle

  const animatedProgress = useSharedValue(0);

  useEffect(() => {
    animatedProgress.value = withTiming(progress, {
      duration: 1000,
      easing: Easing.out(Easing.exp),
    });
  }, [progress]);

  const animatedProps = useAnimatedProps(() => {
    const arcLength = circumference * animatedProgress.value;
    return {
      strokeDashoffset: circumference - arcLength,
    };
  });

  const semicirclePath = `
    M ${cx - r}, ${cy}
    A ${r},${r} 0 1,1 ${cx + r},${cy}
  `;

  return (
    <Svg width={width} height={r + strokeWidth}>
      <Path
        d={semicirclePath}
        stroke="#E6E6E6"
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        fill="transparent"
      />
      <AnimatedPath
        d={semicirclePath}
        stroke="#FFCE01"
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        fill="transparent"
        strokeDasharray={`${circumference}`}
        animatedProps={animatedProps}
      />
    </Svg>
  );
};

const Home = () => {
  const [progress, setProgress] = useState(0); // Initial progress percentage (0-1 scale)
  const { user, isLogged, challenge, setChallenge } = useGlobalContext();

  // Function to increase progress by 10%
  const increaseProgress = () => {
    const newProgress = progress + 0.1;
    setProgress(Math.min(newProgress, 1)); // Limit progress to 100%
  };

  // Function to decrease progress by 10%
  const decreaseProgress = () => {
    const newProgress = progress - 0.1;
    setProgress(Math.max(newProgress, 0)); // Limit progress to 0%
  };

  const renderContent = () => {
    if (challenge === null) {
      return (
        <View style={styles.placeholder}>
          <Text style={styles.placeholderText}>No challenge selected</Text>
        </View>
      );
    } else if (challenge == 1) {
      return <Image source={images.ch1} style={styles.photo} />;
    } else if (challenge == 2) {
      return <Image source={images.ch2} style={styles.photo} />;
    } else if (challenge == 3) {
      return <Image source={images.ch3} style={styles.photo} />;
    }
    return null;
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.title}>Welcome back, {user.name}!</Text>
      {/* Semicircular Progress Bar */}
      <View style={{ marginTop: hp('1%'), marginBottom: -90 }}>
        <SemiCircularProgress progress={progress} />
      </View>

      <Text style={styles.progressText}>{Math.round(progress * 10)/10} Kw</Text>
      <Text style={styles.suggestText} >Adjust your daily goal!</Text>
      {/* Buttons */}
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={decreaseProgress}>
        <Text style={styles.buttonText}>-Decrease</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={increaseProgress}>
        <Text style={styles.buttonText}>+ Increase</Text>
      </TouchableOpacity>

      </View>
      <View style={styles.contentContainer}>{renderContent()}</View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  progressText: {
    fontSize: 60,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: -5,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 5,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  fullWidthButton: {
    width: width - 40,
    height: buttonWidth,
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20,
    borderRadius: 20,
    marginTop: 20,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 4, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 4, // IOS
  },
  buttonText: {
    color: 'black',
    fontSize: 18,
    fontWeight: 'bold',
  },
  contentContainer: {
    marginTop: 20,
    alignItems: 'center',
  },
  photo: {
    width: width - 40,
    height: (width - 40) / 2, // Adjust height as needed
    resizeMode: 'cover',
    borderRadius: 20,
  },
  placeholder: {
    width: width - 40,
    height: (width - 40) / 2, // Adjust height as needed
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E8E8E8',
    borderRadius: 20,
  },
  placeholderText: {
    fontSize: 18,
    color: '#999',
  },
  button: {
    width: buttonWidth,
    height: buttonWidth/1.3,
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 4, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 4, //IOS
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginTop: 20,
  },
  suggestText:{   
    color: 'black',
  fontSize: 18,
  fontWeight: 'bold',
   textAlign: 'center',
  },
});

export default Home;
