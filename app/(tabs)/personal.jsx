import React from 'react';
import { LineChart } from 'react-native-chart-kit';
import { TouchableOpacity, Text, StyleSheet, Dimensions, ScrollView, Image,View } from 'react-native';
import { icons, images } from '../../constants';
import { router } from 'expo-router';

const { width } = Dimensions.get('window');

const personal = () => {
  return (
    <ScrollView style={styles.container}>
      <Text style={styles.header} className="font-pmedium">Personal Statistics</Text>
      <LineChart
        data={{
          //Profile photos of your friends
          labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
          datasets: [
            {
              data: [6, 10, 17, 3, 0, 20, 15],
            },
          ],
        }}
        width={width - 40} // Adjusted width to match the full-width button
        height={width - 40} // Adjusted height to match the full-width button
        yAxisSuffix="cal"
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: '#FF9C01',
          backgroundGradientTo: '#FF9C01',
          decimalPlaces: 0,
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16,
         
          },
        }}
        style={styles.fullWidthButton}
      />
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => router.push('../(personal_tabs)/trophies')}
        >
          <Image
            source={images.throphy} style={styles.plusLogo} />
          <Text style={styles.buttonText} className="font-pmedium">Trophies</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => router.push('../(personal_tabs)/challenge')}>
          <Image
            source={images.swords} style={styles.swords}
            resizeMode='contain' />
          <Text style={styles.buttonText} className="font-pmedium">Challenges</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const buttonWidth = (width - 60) / 2; // Subtracting padding and margins

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontSize: 24,
    marginBottom: 16,
    marginLeft:20
  },
  fullWidthButton: {
    width: width - 40, // Adjust width to match the screen width minus padding
    height: width - 40, // Make the button square
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20, // Add horizontal margin to match the button container
    borderRadius: 20, // Adjust the border radius as needed
    marginTop: 5, // Add margin top for spacing
    marginBottom:-25,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between', // Align buttons with equal spacing between them
    paddingHorizontal: 20, // Add horizontal padding to the button container
    marginTop: 50, // Add margin top for spacing
  },
  plusLogo: {
    width: 80, // Adjust the width and height of the plus logo as needed
    height: 80,
    marginBottom: 10, // Add margin bottom to create space between the logo and the text
    tintColor: 'black',
  },
  swords: {
    width: 90, // Adjust the width and height of the plus logo as needed
    height: 100,
    marginBottom: 5, // Add margin bottom to create space between the logo and the text
    tintColor: 'black',
  },
  button: {
    width: buttonWidth,
    height: buttonWidth,
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 4, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 4, //IOS
  },
  buttonText: {
    color: 'black',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default personal;
