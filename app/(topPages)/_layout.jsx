import { View, Text, StatusBar } from 'react-native'
import React from 'react'
import { Stack } from 'expo-router'


const _layout = () => {
  return (
    <>
    <Stack>
        <Stack.Screen 
            name = "account"
            options={{headerShown: false}}
        />
           <Stack.Screen 
            name = "notifications"
            options={{headerShown: false}}
        />
    </Stack>

    <StatusBar backgroundColor='#FFFFFF' style='light'/>
</>
  )
}

export default _layout