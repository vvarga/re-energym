import React from 'react';
import { View, TouchableOpacity, Dimensions, StyleSheet, Image, Text, ScrollView } from 'react-native';
import { useRouter } from 'expo-router';
import { icons, images } from '../../constants'; // Adjust the path to your icons and images if necessary
import ProfilePicture from '../../lib/ProfilePicture';
import {useImageState} from '../../context/ImageStateContext';
import  { useGlobalContext } from '../../context/GlobalProvider';
import { signOut } from '../../lib/appwrite';

const Account = () => {
  const router = useRouter();
  const { imageUri, setImageUri } = useImageState();
  const { user, setChallenge } = useGlobalContext();
  return (
    <ScrollView contentContainerStyle={styles.container}>
     
      <TouchableOpacity style={styles.topLeftButton} onPress={() => router.back()}>
        <Image source={icons.leftArrow} style={styles.arrowIcon} />
      </TouchableOpacity>
      <View style={styles.centeredContainer}>
      {imageUri ? (
            <Image source={{ uri: imageUri }} style={styles.profileImage} />
          ) : (
            <Image source={images.account_image} style={styles.profileImage} />
          )}
            {user && <ProfilePicture userId={user.$id} />}

        <TouchableOpacity style={styles.fullWidthButton} onPress={ () => {signOut(); setChallenge(null)}}>
        <Text style={{ color: 'red' }}>SIgn Out</Text>

        </TouchableOpacity>
      </View>
      <View style={styles.versionContainer}>
        <Text style={styles.versionText}>Version 0.2</Text>
      </View>
    </ScrollView>
  );
};

const { width } = Dimensions.get('window');
const buttonWidth = (width - 60) / 2; // Subtracting padding and margins

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: 'center',
    paddingBottom: 20, // Add padding at the bottom to accommodate the version text
  },

  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginBottom:100,
  },
  centeredContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  fullWidthButton: {
    width: width - 40,
    height: 60,
    backgroundColor: '#E8E8E8',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 4, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 4, // IOS
  },
  topLeftButton: {
    position: 'absolute',
    top: 10,
    left: 10,
    padding: 15,
    borderRadius: 20,
    zIndex: 1, // Ensure the button is on top
    marginTop:40,
  },
  arrowIcon: {
    width: 20,
    height: 15,
    tintColor: 'black', // Adjust the color if necessary
  },
  versionContainer: {
    position: 'absolute',
    bottom: 10,
  },
  versionText: {
    fontSize: 12,
    color: '#999',
  },
});

export default Account;
