import React from 'react';
import { View, TouchableOpacity, Dimensions, StyleSheet, Image, Text, ScrollView } from 'react-native';
import { useRouter } from 'expo-router';
import { icons, images } from '../../constants'; // Adjust the path to your icons and images if necessary

const notifications = [
  { id: 1, message: "New class added: Fitness" },
  { id: 2, message: "Reminder: Yoga class at 6 PM" },
  { id: 3, message: "Special offer: 20% off on membership renewal" },
  { id: 4, message: "Event tomorrow: Workout monday madness" },
  { id: 5, message: "Monthly free class voucher: expired" },
];

const Notifications = () => {
  const router = useRouter();

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <TouchableOpacity style={styles.topLeftButton} onPress={() => router.back()}>
        <Image source={icons.leftArrow} style={styles.arrowIcon} />
      </TouchableOpacity>
      <Text style={styles.title}>Notifications</Text>
      <View style={styles.centeredContainer}>
      <View style={styles.notificationsContainer}>
        {notifications.map((notification, index) => (
          <View key={notification.id}>
            <Text style={styles.notificationText}>{notification.message}</Text>
            {index < notifications.length - 1 && <View style={styles.separator} />}
          </View>
        ))}
      </View>
      </View>
      <View style={styles.versionContainer}>
        <Text style={styles.versionText}>Version 0.2</Text>
      </View>
    </ScrollView>
  );
};

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingBottom: 20, // Add padding at the bottom to accommodate the version text
  },
  notificationsContainer: {
    flex: 1,
    paddingTop: 60, // Ensure notifications start below the title and back button
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 60,
    marginBottom:5,
  },
  notificationText: {
    fontSize: 20,
    paddingVertical: 10,
  },
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: '#CCC',
  },
  topLeftButton: {
    position: 'absolute',
    top: 10,
    left: 10,
    padding: 15,
    borderRadius: 20,
    zIndex: 1, // Ensure the button is on top
    marginTop: 40,
  },
  arrowIcon: {
    width: 20,
    height: 15,
    tintColor: 'black', // Adjust the color if necessary
  },
  versionContainer: {
    position: 'absolute',
    bottom: 10,
    alignSelf: 'center',
  },
  versionText: {
    fontSize: 12,
    color: '#999',
  },
  centeredContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
});

export default Notifications;
