import { View, Text, StatusBar } from 'react-native'
import React from 'react'
import { Stack } from 'expo-router'


const _layout = () => {
  return (
    <>
    <Stack>
        <Stack.Screen 
            name = "abs"
            options={{headerShown: false}}
        />
           <Stack.Screen 
            name = "back"
            options={{headerShown: false}}
        />
            <Stack.Screen 
            name = "bike"
            options={{headerShown: false}}
        />
            <Stack.Screen 
            name = "chest"
            options={{headerShown: false}}
        />
            <Stack.Screen 
            name = "endurance"
            options={{headerShown: false}}
        />
            <Stack.Screen 
            name = "glutes"
            options={{headerShown: false}}
        />
            <Stack.Screen 
            name = "pushup"
            options={{headerShown: false}}
        />
            <Stack.Screen 
            name = "stairmaster"
            options={{headerShown: false}}
        />
             <Stack.Screen 
            name = "treadmill"
            options={{headerShown: false}}
        />
    </Stack>

    <StatusBar backgroundColor='#FFFFFF' style='light'/>
</>
  )
}

export default _layout