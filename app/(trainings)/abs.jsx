import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const AbWorkout = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.header}>20 MIN ABS</Text>

            <Text style={styles.subHeader}>Warm-Up (2 minutes)</Text>
            <Text style={styles.interval}>0:00 - 2:00: Perform light cardio and dynamic stretches to prepare your core. Examples include marching in place, high knees, and arm circles.</Text>

            <Text style={styles.subHeader}>Workout (16 minutes)</Text>
            <Text style={styles.details}>Perform each exercise for 1 minute, with minimal rest in between. If needed, rest for 15-30 seconds between exercises.</Text>
            
            <Text style={styles.interval}>Plank\n2:00 - 3:00: Hold a plank position, keeping your body in a straight line from head to heels. Engage your core and avoid letting your hips sag.</Text>
            <Text style={styles.interval}>Bicycle Crunches\n3:00 - 4:00: Lie on your back with hands behind your head, lift your shoulders off the ground, and bring one knee towards your chest while twisting your opposite elbow towards it. Alternate sides.</Text>
            <Text style={styles.interval}>Leg Raises\n4:00 - 5:00: Lie on your back with legs straight. Lift your legs towards the ceiling while keeping them straight, then slowly lower them back down without touching the ground.</Text>
            <Text style={styles.interval}>Russian Twists\n5:00 - 6:00: Sit on the floor with your knees bent and lean back slightly. Hold a weight or medicine ball, and twist your torso to the left and right, touching the weight to the floor on each side.</Text>
            <Text style={styles.interval}>Mountain Climbers\n6:00 - 7:00: Get into a plank position and alternate bringing your knees towards your chest as if you are running in place.</Text>
            <Text style={styles.interval}>Flutter Kicks\n7:00 - 8:00: Lie on your back with legs straight. Lift your legs slightly off the ground and quickly alternate small up-and-down kicks.</Text>
            <Text style={styles.interval}>Side Plank (Left)\n8:00 - 9:00: Lie on your side and prop yourself up on one elbow, keeping your body in a straight line. Hold this position, engaging your obliques.</Text>
            <Text style={styles.interval}>Side Plank (Right)\n9:00 - 10:00: Repeat the side plank on your right side.</Text>
            <Text style={styles.interval}>Reverse Crunches\n10:00 - 11:00: Lie on your back with knees bent and feet off the ground. Lift your hips towards your chest, using your lower abs, then lower back down.</Text>
            <Text style={styles.interval}>Toe Touches\n11:00 - 12:00: Lie on your back with legs straight up towards the ceiling. Reach your hands towards your toes, lifting your shoulders off the ground.</Text>
            <Text style={styles.interval}>Scissor Kicks\n12:00 - 13:00: Lie on your back with legs straight. Lift your legs slightly off the ground and cross them over each other in a scissor motion.</Text>
            <Text style={styles.interval}>V-Ups\n13:00 - 14:00: Lie on your back with arms extended overhead. Simultaneously lift your legs and torso towards each other, forming a V shape with your body.</Text>
            <Text style={styles.interval}>Plank with Shoulder Taps\n14:00 - 15:00: Get into a plank position. Tap your right shoulder with your left hand, then your left shoulder with your right hand, alternating sides.</Text>
            <Text style={styles.interval}>Dead Bug\n15:00 - 16:00: Lie on your back with arms extended towards the ceiling and knees bent at 90 degrees. Slowly lower your right arm and left leg towards the floor while keeping your back flat, then return to the starting position and switch sides.</Text>

            <Text style={styles.subHeader}>Cool-Down (2 minutes)</Text>
            <Text style={styles.interval}>16:00 - 18:00: Perform static stretching focusing on the core. Stretch your lower back, obliques, and hip flexors. Hold each stretch for 20-30 seconds.</Text>

            <Text style={styles.tipsHeader}>Tips:</Text>
            <Text style={styles.tips}>Adjust as Needed: Modify the exercises or take brief rests if you find any exercise too challenging.</Text>
            <Text style={styles.tips}>Stay Hydrated: Drink water before and after your workout.</Text>
            <Text style={styles.tips}>Proper Form: Focus on maintaining proper form to maximize effectiveness and prevent injury.</Text>
            <Text style={styles.tips}>Listen to Your Body: If you experience any pain or discomfort, stop and adjust your form or take a break.</Text>
            <Text style={styles.tips}>Enjoy your ab workout!</Text>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20,
        marginTop: 30,
    },
    subHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 10,
        color: 'orange',
    },
    details: {
        fontSize: 16,
        marginBottom: 5,
    },
    interval: {
        fontSize: 16,
        marginBottom: 5,
    },
    tipsHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 5,
        color: 'orange',
    },
    tips: {
        fontSize: 14,
        marginBottom: 5,
    }
});

export default AbWorkout;