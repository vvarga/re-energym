import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const BackWorkout = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.header}>15 MIN FULL BACK</Text>

            <Text style={styles.subHeader}>Warm-Up (2 minutes)</Text>
            <Text style={styles.interval}>0:00 - 2:00: Perform dynamic stretches and light cardio. Examples include arm circles, arm swings, and a few minutes on a rowing machine to get your back muscles warmed up.</Text>

            <Text style={styles.subHeader}>Workout (12 minutes)</Text>
            <Text style={styles.details}>Perform each exercise for 1 minute, with minimal rest (15-30 seconds) in between exercises.</Text>
            
            <Text style={styles.interval}>Lat Pulldowns\n2:00 - 3:00: Sit at a lat pulldown machine. Pull the bar down to your chest, squeezing your shoulder blades together, then slowly return to the starting position.</Text>
            <Text style={styles.interval}>Bent Over Rows\n3:00 - 4:00: Hold a barbell or dumbbells with an overhand grip. Bend at your hips and knees, keeping your back straight. Pull the weight towards your torso, squeezing your shoulder blades together, then lower it back down.</Text>
            <Text style={styles.interval}>Seated Cable Rows\n4:00 - 5:00: Sit at a cable row machine with your feet on the foot platform. Pull the handle towards your torso, keeping your back straight and elbows close to your body, then return to the starting position.</Text>
            <Text style={styles.interval}>Single-Arm Dumbbell Rows (Right)\n5:00 - 6:00: Place your left knee and hand on a bench, holding a dumbbell in your right hand. Pull the dumbbell towards your hip, squeezing your shoulder blade, then lower it back down.</Text>
            <Text style={styles.interval}>Single-Arm Dumbbell Rows (Left)\n6:00 - 7:00: Repeat the single-arm dumbbell rows with your left arm.</Text>
            <Text style={styles.interval}>Face Pulls\n7:00 - 8:00: Use a cable machine with a rope attachment set at face height. Pull the rope towards your face, keeping your elbows high and squeezing your shoulder blades together, then return to the starting position.</Text>
            <Text style={styles.interval}>Back Extensions\n8:00 - 9:00: Use a back extension bench or machine. Cross your arms over your chest and lower your upper body towards the floor, then lift back up, squeezing your glutes and lower back muscles.</Text>
            <Text style={styles.interval}>T-Bar Rows\n9:00 - 10:00: Stand over a T-bar row machine with your feet shoulder-width apart. Pull the weight towards your chest, squeezing your shoulder blades together, then lower it back down.</Text>
            <Text style={styles.interval}>Reverse Flyes\n10:00 - 11:00: Hold a pair of dumbbells and bend at your hips with your back straight. Lift the weights out to the sides, squeezing your shoulder blades together, then lower them back down.</Text>
            <Text style={styles.interval}>Deadlifts\n11:00 - 12:00: Stand with your feet shoulder-width apart, holding a barbell in front of you. Bend at your hips and knees to lower the barbell to the floor, then stand back up, keeping your back straight and driving through your heels.</Text>

            <Text style={styles.subHeader}>Cool-Down (1 minute)</Text>
            <Text style={styles.interval}>12:00 - 13:00: Perform static stretching focusing on your back. Stretch your upper and lower back, as well as your shoulders and lats. Hold each stretch for 20-30 seconds.</Text>

            <Text style={styles.tipsHeader}>Tips:</Text>
            <Text style={styles.tips}>Adjust Weights: Use weights that challenge you but allow you to maintain proper form. As you progress, gradually increase the weights.</Text>
            <Text style={styles.tips}>Stay Hydrated: Keep a water bottle nearby and drink water throughout your workout.</Text>
            <Text style={styles.tips}>Proper Form: Focus on maintaining proper form for each exercise to maximize effectiveness and prevent injury.</Text>
            <Text style={styles.tips}>Listen to Your Body: If you experience any pain or discomfort, stop and adjust your form or take a break.</Text>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20,
        marginTop: 30,
    },
    subHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 10,
        color: 'orange',
    },
    details: {
        fontSize: 16,
        marginBottom: 5,
    },
    interval: {
        fontSize: 16,
        marginBottom: 5,
    },
    tipsHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 5,
        color: 'orange',
    },
    tips: {
        fontSize: 14,
        marginBottom: 5,
    }
});

export default BackWorkout;
