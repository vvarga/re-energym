import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const Bike = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.title }>20 MIN BIKE</Text>
            <Text style={styles.subHeader}>Warm-Up (4 minutes)</Text>
            <Text style={styles.interval}>0:00 - 4:00: Pedal at an easy pace with low resistance. Aim for a cadence of 80-90 RPM (revolutions per minute).</Text>

            <Text style={styles.subHeader}>Interval Training (14 minutes)</Text>
            <Text style={styles.interval}>4:00 - 6:00: Increase resistance to a moderate level and pedal at a faster pace (90-100 RPM).</Text>
            <Text style={styles.interval}>6:00 - 8:00: Increase resistance slightly and maintain a steady, challenging pace (80-90 RPM).</Text>
            <Text style={styles.interval}>8:00 - 9:00: Increase resistance to a high level and pedal at a high-intensity pace (70-80 RPM).</Text>
            <Text style={styles.interval}>9:00 - 10:00: Decrease resistance to a moderate level and pedal at a recovery pace (80-90 RPM).</Text>
            <Text style={styles.interval}>10:00 - 12:00: Increase resistance to a high level and pedal at a high-intensity pace (70-80 RPM).</Text>
            <Text style={styles.interval}>12:00 - 13:00: Decrease resistance to a low level and pedal at a recovery pace (90-100 RPM).</Text>
            <Text style={styles.interval}>13:00 - 15:00: Increase resistance to a high level and pedal at a high-intensity pace (70-80 RPM).</Text>
            <Text style={styles.interval}>15:00 - 16:00: Decrease resistance to a low level and pedal at a recovery pace (90-100 RPM).</Text>
            <Text style={styles.interval}>16:00 - 18:00: Increase resistance to a high level and pedal at a high-intensity pace (70-80 RPM).</Text>

            <Text style={styles.subHeader}>Cool-Down (2 minutes)</Text>
            <Text style={styles.interval}>18:00 - 20:00: Decrease resistance to a low level and pedal at an easy pace (80-90 RPM).</Text>

            <Text style={styles.tipsHeader}>Tips:</Text>
            <Text style={styles.tips}>Adjust as Needed: Modify the resistance and pace based on your fitness level.</Text>
            <Text style={styles.tips}>Stay Hydrated: Keep a water bottle nearby and take small sips as needed.</Text>
            <Text style={styles.tips}>Listen to Your Body: If at any point you feel overly fatigued or uncomfortable, reduce the intensity or take a short break.</Text>
            <Text style={styles.tips}>Proper Form: Ensure you maintain proper form, keeping your back straight and core engaged.</Text>
            
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 30,
        marginBottom: 30,
      },
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    subHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'orange',
        marginBottom: 10,
    },
    interval: {
        fontSize: 16,
        marginBottom: 5,
    },
    tipsHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 5,
        color: 'orange',
    },
    tips: {
        fontSize: 14,
        marginBottom: 5,
    }
});

export default Bike;
