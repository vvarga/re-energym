import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const ChestWorkout = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.header}>30 MIN CHEST</Text>

            <Text style={styles.subHeader}>Warm-Up (5 minutes)</Text>
            <Text style={styles.interval}>0:00 - 5:00: Perform dynamic stretches and light cardio to get your blood flowing. Examples include arm circles, shoulder rolls, and a few minutes on a treadmill or stationary bike.</Text>

            <Text style={styles.subHeader}>Workout (23 minutes)</Text>
            <Text style={styles.details}>Perform each exercise for the specified number of sets and reps, taking short breaks (30-60 seconds) between sets.</Text>
            
            <Text style={styles.interval}>Flat Bench Press\nSets: 3\nReps: 10-12\nFocus: Lie on a flat bench with feet flat on the floor. Grip the barbell slightly wider than shoulder-width apart. Lower the bar to your chest, then press it back up.</Text>
            <Text style={styles.interval}>Incline Dumbbell Press\nSets: 3\nReps: 10-12\nFocus: Set an adjustable bench to a 30-45 degree incline. Hold a dumbbell in each hand, palms facing forward. Press the dumbbells up until your arms are fully extended, then lower them back down to chest level.</Text>
            <Text style={styles.interval}>Decline Bench Press\nSets: 3\nReps: 10-12\nFocus: Set an adjustable bench to a decline position. Grip the barbell slightly wider than shoulder-width apart. Lower the bar to your lower chest, then press it back up.</Text>
            <Text style={styles.interval}>Chest Flyes\nSets: 3\nReps: 12-15\nFocus: Lie on a flat bench with a dumbbell in each hand, palms facing each other. With a slight bend in your elbows, lower the dumbbells out to the sides until you feel a stretch in your chest, then bring them back together above your chest.</Text>
            <Text style={styles.interval}>Cable Crossovers\nSets: 3\nReps: 12-15\nFocus: Set the pulleys on a cable machine to the highest setting. Grab the handles and step forward, bringing your hands together in front of your body. Slowly return to the starting position.</Text>
            <Text style={styles.interval}>Push-Ups\nSets: 2\nReps: To Failure\nFocus: Perform standard push-ups with your hands placed slightly wider than shoulder-width apart. Lower your body until your chest nearly touches the ground, then push back up.</Text>

            <Text style={styles.subHeader}>Cool-Down (2 minutes)</Text>
            <Text style={styles.interval}>28:00 - 30:00: Perform static stretching focusing on your chest, shoulders, and triceps. Hold each stretch for 20-30 seconds to help with muscle recovery and flexibility.</Text>

            <Text style={styles.tipsHeader}>Tips:</Text>
            <Text style={styles.tips}>Adjust Weights: Use weights that challenge you but allow you to maintain proper form. As you progress, gradually increase the weights.</Text>
            <Text style={styles.tips}>Stay Hydrated: Keep a water bottle nearby and drink water throughout your workout.</Text>
            <Text style={styles.tips}>Proper Form: Focus on maintaining proper form for each exercise to maximize effectiveness and prevent injury. If you're unsure about form, ask a trainer for guidance.</Text>
            <Text style={styles.tips}>Listen to Your Body: If you experience any pain or discomfort, stop and adjust your form or take a break.</Text>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20,
        marginTop: 30,
    },
    subHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 5,
        marginBottom: 10,
        color: 'orange',
    },
    details: {
        fontSize: 16,
        marginBottom: 5,
    },
    interval: {
        fontSize: 16,
        marginBottom: 5,
    },
    tipsHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 5,
        color: 'orange',
    },
    tips: {
        fontSize: 14,
        marginBottom: 5,
    }
});

export default ChestWorkout;
