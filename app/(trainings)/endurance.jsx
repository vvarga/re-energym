import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const Endurance = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.header}>30 MIN ENDURANCE</Text>

            <Text style={styles.subHeader}>Warm-Up (5 minutes)</Text>
            <Text style={styles.interval}>0:00 - 5:00: Perform dynamic stretches and light cardio (such as jogging in place, jumping jacks, or using a stationary bike).</Text>

            <Text style={styles.subHeader}>Circuit Training (20 minutes)</Text>
            <Text style={styles.details}>Perform each exercise for 1 minute, then move to the next exercise with minimal rest. Complete the circuit twice:</Text>
            <Text style={styles.interval}>Treadmill Run/Walk: Set the treadmill to a moderate pace and jog or walk briskly.</Text>
            <Text style={styles.interval}>Push-Ups: Perform push-ups at a steady pace. Modify by doing them on your knees if needed.</Text>
            <Text style={styles.interval}>Jump Rope: Jump rope at a moderate to fast pace. If you don't have a jump rope, perform imaginary jump rope.</Text>
            <Text style={styles.interval}>Dumbbell Squats: Hold a pair of dumbbells at your sides and perform squats.</Text>
            <Text style={styles.interval}>Rowing Machine: Row at a steady pace. Focus on maintaining good form.</Text>
            <Text style={styles.interval}>Dumbbell Shoulder Press: Hold a pair of dumbbells and perform shoulder presses.</Text>
            <Text style={styles.interval}>Stationary Bike: Pedal at a moderate pace, adjusting resistance to keep it challenging.</Text>
            <Text style={styles.interval}>Plank: Hold a plank position, keeping your body straight and core engaged.</Text>
            <Text style={styles.interval}>Step-Ups: Step up onto a bench or box, alternating legs.</Text>
            <Text style={styles.interval}>Kettlebell Swings: Perform kettlebell swings with a weight you can handle safely.</Text>

            <Text style={styles.subHeader}>Cool-Down (5 minutes)</Text>
            <Text style={styles.interval}>25:00 - 30:00: Perform static stretches focusing on major muscle groups, such as hamstrings, quads, calves, chest, and shoulders. Hold each stretch for about 20-30 seconds.</Text>

            <Text style={styles.tipsHeader}>Tips:</Text>
            <Text style={styles.tips}>Adjust Intensity: Modify the weight, speed, or duration of exercises to match your fitness level.</Text>
            <Text style={styles.tips}>Stay Hydrated: Keep a water bottle nearby and take small sips as needed.</Text>
            <Text style={styles.tips}>Listen to Your Body: If at any point you feel overly fatigued or uncomfortable, reduce the intensity or take a short break.</Text>
            <Text style={styles.tips}>Proper Form: Focus on maintaining proper form for each exercise to prevent injury.</Text>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20,
        marginTop: 40,
       
    },
    subHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 10,
        color: 'orange',
    },
    details: {
        fontSize: 16,
        marginBottom: 5,
    },
    interval: {
        fontSize: 16,
        marginBottom: 5,
    },
    tipsHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 5,
        color:'orange',
    },
    tips: {
        fontSize: 14,
        marginBottom: 5,
        
    }
});

export default Endurance;
