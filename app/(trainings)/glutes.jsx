import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const GlutesWorkout = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.header}>40 MIN GLUTES</Text>

            <Text style={styles.subHeader}>Warm-Up (5 minutes)</Text>
            <Text style={styles.interval}>0:00 - 5:00: Dynamic stretches and light cardio. Perform leg swings, bodyweight squats, and lunges to get your muscles warmed up and ready for the workout.</Text>

            <Text style={styles.subHeader}>Workout (30 minutes)</Text>
            <Text style={styles.details}>Perform each exercise for the specified number of sets and reps, taking short breaks (30-60 seconds) between sets.</Text>
            
            <Text style={styles.interval}>Barbell Squats\nSets: 3\nReps: 12\nFocus: Lower yourself until your thighs are parallel to the ground, then push through your heels to stand back up. Keep your back straight and core engaged.</Text>
            <Text style={styles.interval}>Romanian Deadlifts\nSets: 3\nReps: 12\nFocus: With a slight bend in your knees, hinge at the hips to lower the barbell down your legs, then return to standing. Keep your back straight and push your hips forward as you lift.</Text>
            <Text style={styles.interval}>Hip Thrusts\nSets: 3\nReps: 15\nFocus: Place your upper back on a bench and your feet flat on the floor. Thrust your hips up, squeezing your glutes at the top, then lower back down.</Text>
            <Text style={styles.interval}>Bulgarian Split Squats\nSets: 3 per leg\nReps: 12\nFocus: Place one foot on a bench behind you and lower your body until your front thigh is parallel to the ground. Push through your front heel to stand back up.</Text>
            <Text style={styles.interval}>Cable Kickbacks\nSets: 3 per leg\nReps: 15\nFocus: Attach a cable to your ankle and kick your leg back, squeezing your glute at the top. Return to the starting position and repeat.</Text>
            <Text style={styles.interval}>Step-Ups\nSets: 3 per leg\nReps: 12\nFocus: Step up onto a bench or box with one foot, driving through your heel to lift your body up. Step down and repeat on the other leg.</Text>
            <Text style={styles.interval}>Glute Bridges\nSets: 3\nReps: 15\nFocus: Lie on your back with your feet flat on the floor and knees bent. Lift your hips towards the ceiling, squeezing your glutes at the top, then lower back down.</Text>

            <Text style={styles.subHeader}>Cool-Down (5 minutes)</Text>
            <Text style={styles.interval}>35:00 - 40:00: Static stretching focusing on the glutes, hamstrings, and lower back. Hold each stretch for 20-30 seconds to help with muscle recovery and flexibility.</Text>

            <Text style={styles.tipsHeader}>Tips:</Text>
            <Text style={styles.tips}>Adjust Weights: Use weights that challenge you but allow you to maintain proper form. As you progress, gradually increase the weights.</Text>
            <Text style={styles.tips}>Stay Hydrated: Keep a water bottle nearby and drink water throughout your workout.</Text>
            <Text style={styles.tips}>Proper Form: Focus on maintaining proper form to maximize effectiveness and prevent injury. If you're unsure about form, ask a trainer for guidance.</Text>
            <Text style={styles.tips}>Listen to Your Body: If you experience any pain or discomfort, stop and adjust your form or take a break.</Text>
            <Text style={styles.tips}>Enjoy your glutes workout!</Text>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20,
        marginTop: 30,
    },
    subHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 10,
        color: 'orange',
    },
    details: {
        fontSize: 16,
        marginBottom: 5,
    },
    interval: {
        fontSize: 16,
        marginBottom: 5,
    },
    tipsHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 5,
        color: 'orange',
    },
    tips: {
        fontSize: 14,
        marginBottom: 5,
    }
});

export default GlutesWorkout;
