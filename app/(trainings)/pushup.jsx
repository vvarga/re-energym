import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const PushUpWorkout = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.header}>8 MIN PUSH UP</Text>

            <Text style={styles.subHeader}>Warm-Up (1 minute)</Text>
            <Text style={styles.interval}>0:00 - 1:00: Perform dynamic stretches focusing on your arms, shoulders, and chest. Arm circles, shoulder rolls, and light jogging in place are good options.</Text>

            <Text style={styles.subHeader}>Push-Up Circuit (6 minutes)</Text>
            <Text style={styles.details}>Perform each push-up variation for 1 minute. If you need a break, rest for a few seconds and then continue. Adjust the difficulty by performing the push-ups on your knees if needed.</Text>
            
            <Text style={styles.interval}>Standard Push-Ups (1 minute): Keep your body in a straight line from head to heels, lower yourself until your chest nearly touches the floor, and push back up.</Text>
            <Text style={styles.interval}>Wide-Grip Push-Ups (1 minute): Place your hands wider than shoulder-width apart. This targets the chest muscles more intensely.</Text>
            <Text style={styles.interval}>Diamond Push-Ups (1 minute): Place your hands close together under your chest, forming a diamond shape with your thumbs and index fingers. This targets the triceps.</Text>
            <Text style={styles.interval}>Decline Push-Ups (1 minute): Place your feet on an elevated surface such as a bench or step, and perform push-ups. This targets the upper chest and shoulders.</Text>
            <Text style={styles.interval}>Close-Grip Push-Ups (1 minute): Place your hands directly under your shoulders, keeping your elbows close to your body as you lower and push up. This targets the triceps and inner chest.</Text>
            <Text style={styles.interval}>Plyometric Push-Ups (1 minute): Push up explosively so your hands leave the ground, then catch yourself and lower into the next rep. This increases power and explosiveness.</Text>

            <Text style={styles.subHeader}>Cool-Down (1 minute)</Text>
            <Text style={styles.interval}>7:00 - 8:00: Perform static stretches for your arms, shoulders, and chest. Hold each stretch for about 20-30 seconds.</Text>

            <Text style={styles.tipsHeader}>Tips:</Text>
            <Text style={styles.tips}>Adjust as Needed: If a full minute of any variation is too challenging, take brief rests and then continue.</Text>
            <Text style={styles.tips}>Stay Hydrated: Drink water as needed before and after the workout.</Text>
            <Text style={styles.tips}>Proper Form: Focus on maintaining proper form for each push-up variation to maximize effectiveness and prevent injury.</Text>
            <Text style={styles.tips}>Listen to Your Body: If you experience any pain or discomfort, stop and adjust your form or take a break.</Text>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20,
        marginTop: 40,
    },
    subHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 10,
        color: 'orange',
    },
    details: {
        fontSize: 16,
        marginBottom: 5,
    },
    interval: {
        fontSize: 16,
        marginBottom: 5,
    },
    tipsHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 5,
        color: 'orange',
    },
    tips: {
        fontSize: 14,
        marginBottom: 5,
    }
});

export default PushUpWorkout;
