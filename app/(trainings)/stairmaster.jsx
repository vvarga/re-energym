import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const Stairmaster = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.title}>45 MIN STAIRMASTER</Text>
            <Text style={styles.subHeader}>Warm-Up (5 minutes)</Text>
            <Text style={styles.interval}>0:00 - 5:00: Begin at a low intensity, using Level 2-4. Focus on warming up your legs and getting your heart rate up gradually.</Text>

            <Text style={styles.subHeader}>Steady-State Training (10 minutes)</Text>
            <Text style={styles.interval}>5:00 - 15:00: Increase to a moderate intensity, using Level 5-7. Maintain a steady pace, focusing on a consistent climb without overexerting yourself.</Text>

            <Text style={styles.subHeader}>Interval Training (25 minutes)</Text>
            <Text style={styles.interval}>15:00 - 17:00: Increase to a high intensity, using Level 8-10. Climb at a vigorous pace.</Text>
            <Text style={styles.interval}>17:00 - 19:00: Decrease to a moderate intensity, using Level 5-6. Recover at a comfortable pace.</Text>
            <Text style={styles.interval}>19:00 - 21:00: Increase to a high intensity, using Level 8-10. Climb at a vigorous pace.</Text>
            <Text style={styles.interval}>21:00 - 23:00: Decrease to a moderate intensity, using Level 5-6. Recover at a comfortable pace.</Text>
            <Text style={styles.interval}>23:00 - 25:00: Increase to a high intensity, using Level 8-10. Climb at a vigorous pace.</Text>
            <Text style={styles.interval}>25:00 - 27:00: Decrease to a moderate intensity, using Level 5-6. Recover at a comfortable pace.</Text>
            <Text style={styles.interval}>27:00 - 29:00: Increase to a high intensity, using Level 8-10. Climb at a vigorous pace.</Text>
            <Text style={styles.interval}>29:00 - 31:00: Decrease to a moderate intensity, using Level 5-6. Recover at a comfortable pace.</Text>
            <Text style={styles.interval}>31:00 - 33:00: Increase to a high intensity, using Level 8-10. Climb at a vigorous pace.</Text>
            <Text style={styles.interval}>33:00 - 35:00: Decrease to a moderate intensity, using Level 5-6. Recover at a comfortable pace.</Text>
            <Text style={styles.interval}>35:00 - 37:00: Increase to a high intensity, using Level 8-10. Climb at a vigorous pace.</Text>
            <Text style={styles.interval}>37:00 - 40:00: Decrease to a low intensity, using Level 4-5. Recover at a comfortable pace.</Text>

            <Text style={styles.subHeader}>Cool-Down (5 minutes)</Text>
            <Text style={styles.interval}>40:00 - 45:00: Gradually decrease to a low intensity, using Level 2-3. Focus on bringing your heart rate down and cooling down your muscles.</Text>

            <Text style={styles.tipsHeader}>Tips:</Text>
            <Text style={styles.tips}>Adjust as Needed: Modify the levels based on your fitness level.</Text>
            <Text style={styles.tips}>Stay Hydrated: Keep a water bottle nearby and take small sips as needed.</Text>
            <Text style={styles.tips}>Listen to Your Body: If at any point you feel overly fatigued or uncomfortable, reduce the intensity or take a short break.</Text>
            <Text style={styles.tips}>Proper Form: Ensure you maintain proper form by keeping your back straight, engaging your core, and using the handrails for balance if needed, but avoid leaning heavily on them.</Text>
            <Text style={styles.tips}>Enjoy your workout!</Text>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 30,
        marginBottom: 30,
      },
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    subHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'orange',
        marginBottom: 10,
    },
    interval: {
        fontSize: 16,
        marginBottom: 5,
    },
    tipsHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 5,
        color: 'orange',
    },
    tips: {
        fontSize: 14,
        marginBottom: 5,
    }

});

export default Stairmaster;