import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

const Treadmill = () => {
    return (
        <ScrollView style={styles.container}>
             <Text style={styles.title }>30 MIN TREADMILL</Text>
            <Text style={styles.header}>Warm-Up (5 minutes)</Text>
            <Text style={styles.interval}>0:00 - 5:00: Walk at a comfortable pace (2.5-3.5 mph) with a slight incline (1-2%).</Text>

            <Text style={styles.header}>Interval Training (20 minutes)</Text>
            <Text style={styles.interval}>5:00 - 7:00: Jog at a moderate pace (4.5-5.5 mph) with no incline.</Text>
            <Text style={styles.interval}>7:00 - 9:00: Increase speed to a fast jog or slow run (5.5-6.5 mph) with a slight incline (2%).</Text>
            <Text style={styles.interval}>9:00 - 11:00: Return to a moderate jog (4.5-5.5 mph) with no incline.</Text>
            <Text style={styles.interval}>11:00 - 13:00: Increase speed to a fast jog or slow run (5.5-6.5 mph) with an incline (2%).</Text>
            <Text style={styles.interval}>13:00 - 15:00: Increase speed to a run (6.5-7.5 mph) with a slight incline (2%).</Text>
            <Text style={styles.interval}>15:00 - 17:00: Return to a moderate jog (4.5-5.5 mph) with no incline.</Text>
            <Text style={styles.interval}>17:00 - 19:00: Increase speed to a fast jog or slow run (5.5-6.5 mph) with an incline (3%).</Text>
            <Text style={styles.interval}>19:00 - 21:00: Increase speed to a run (6.5-7.5 mph) with a slight incline (3%).</Text>
            <Text style={styles.interval}>21:00 - 23:00: Return to a moderate jog (4.5-5.5 mph) with no incline.</Text>
            <Text style={styles.interval}>23:00 - 25:00: Increase speed to a fast jog or slow run (5.5-6.5 mph) with an incline (3%).</Text>

            <Text style={styles.header}>Cool-Down (5 minutes)</Text>
            <Text style={styles.interval}>25:00 - 27:00: Decrease speed to a walk (3.0-4.0 mph) with no incline.</Text>
            <Text style={styles.interval}>27:00 - 30:00: Continue walking at a comfortable pace (2.5-3.5 mph) with no incline.</Text>

            <Text style={styles.tipsHeader}>Tips:</Text>
            <Text style={styles.tips}>Adjust as Needed: Modify the speeds and inclines based on your fitness level.</Text>
            <Text style={styles.tips}>Stay Hydrated: Keep a water bottle nearby and take small sips as needed.</Text>
            <Text style={styles.tips}>Listen to Your Body: If at any point you feel dizzy or overly fatigued, slow down or stop and take a break.</Text>
            <Text style={styles.tips}>Safety First: Ensure your treadmill is in good working condition and that you use the safety clip.</Text>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 35,
      },
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    header: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 10,
        color: 'orange'
    },
    interval: {
        fontSize: 16,
        marginBottom: 5,
    },
    tipsHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 5,
        color: 'orange'
    },
    tips: {
        fontSize: 14,
        marginBottom: 5,
        
    }
});

export default Treadmill;