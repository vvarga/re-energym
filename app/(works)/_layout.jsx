import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { View } from 'react-native';
import { AntDesign, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import Cardio from './cardio';
import Bweight from './bweight';
import Weightlift from './weightlift';
const BottomTabNavigator = createBottomTabNavigator();

const screenOptions = {
  tabBarShowLabel: false,
  headerShown: false,
  tabBarActiveTintColor: '#FFA001',
  tabBarInactiveTintColor: '#949191',
  tabBarStyle: {
    position: "absolute",
    elevation: 0,
    backgroundColor: "#fff",
    borderTopWidth: 1,
    borderTopColor: '#E8E8E8',
    height: 95,
  },
};

export default function WorkTab() {
  return (
      <BottomTabNavigator.Navigator screenOptions={screenOptions}>
        <BottomTabNavigator.Screen 
          name="Cardio" 
          component={Cardio} 
          options={{
            tabBarIcon: ({ focused, color }) => (
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <AntDesign name="hearto" size={24} color={color} />
              </View>
            ),
          }} 
        />
        <BottomTabNavigator.Screen 
          name="Bweight" 
          component={Bweight} 
          options={{
            tabBarIcon: ({ focused, color }) => (
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <MaterialIcons name="sports-gymnastics" size={24} color={color} />
              </View>
            ),
          }} 
        />
        <BottomTabNavigator.Screen 
          name
          ="Weightlift" 
          component={Weightlift} 
          options={{
            tabBarIcon: ({ focused, color }) => (
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <MaterialCommunityIcons name="weight-lifter" size={24} color={color} />
              </View>
            ),
          }} 
        />
      </BottomTabNavigator.Navigator>
    
  );
}