import { View, TouchableOpacity, Text, StyleSheet , Dimensions, ScrollView, Image  } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import {images,icons} from '../../constants';
import { router } from 'expo-router';



const { width } = Dimensions.get('window');

const Bweight = () => {
  
  
  return (
    <SafeAreaView style={styles.container}>
    <ScrollView>
      
    <Text style={styles.title}>Body Weight</Text>
    <View style={styles.registrationContainer}>
       <Text style= {{fontSize:20, marginTop:35}} className= " text-lg font-psemibold text-secondary text-center"> Click to see workout </Text>
    </View>
    <TouchableOpacity style={styles.topLeftButton} onPress={() => router.dismiss()}>
        <Image source={icons.leftArrow} style={styles.arrowIcon} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.fullWidthShortButton} className = "mt-0.5 px-4 h-10" onPress={()=>router.push('/endurance')}>
        <Image 
                source={images.endurance}
               
                 
              />
      </TouchableOpacity>
        <TouchableOpacity style={styles.fullWidthShortButton}onPress={()=>router.push('../(trainings)/pushup')}>
          <Image 
                source={images.push}
                
              
              />
        </TouchableOpacity>
        <TouchableOpacity style={styles.fullWidthShortButton} onPress={()=>router.push('../(trainings)/abs')}>
          <Image 
                source={images.abs2}
             
          />
        </TouchableOpacity>    
    </ScrollView>
  </SafeAreaView>
);
};


const buttonWidth = (width - 70) / 2; // Subtracting padding and margins
const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
  },
  fullWidthShortButton: {
    width: width - 40, // Adjust width to match the screen width minus padding
    height:buttonWidth, // Make the button square
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 25, // Add horizontal margin to match the button container
    paddingHorizontal: 15,
    borderRadius: 30, // Adjust the border radius as needed
    marginTop: 50, // Add margin top for spacing
  
  },
  topLeftButton: {
    position: 'absolute',
    top: 5,
    left: 10,
    padding: 15,
    borderRadius: 20,
    zIndex: 1, // Ensure the button is on top
    marginTop:20,
  },
  arrowIcon: {
    width: 20,
    height: 15,
    tintColor: 'black', // Adjust the color if necessary
  },
});
export default Bweight

