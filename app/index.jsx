import { StatusBar } from "expo-status-bar";
import { Redirect, router } from "expo-router";
import { View, Text, Image, ScrollView } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

import { images } from "../constants";
import CustomButtom from '../components/CustomButtom';



const Welcome = () => {
  return (
    <SafeAreaView className="bg-primary h-full">

      <ScrollView
        contentContainerStyle={{
          height: "100%",
        }}
      >
        <View className="w-full justified-center min-h-[83vh] px-4 my-6">
        <Image 
                source={images.logo}
                className = 'w-[230px] h-[300px]'
                style={{marginLeft:60}}
            /> 

    

          <Text className="text-l font-pregular text-gray-100 mt-10 text-center bottom-30">
            Where Creativity Meets Innovation: Embark on a Journey of Alright
            Exploration with Energym
          </Text>

          <CustomButtom
            title="Continue with Email"
            handlePress={() => router.push("/sign-in")}
            containerStyles="w-full mt-9"
          />
        </View>
      </ScrollView>

      <StatusBar backgroundColor="#161622" style="light" />
    </SafeAreaView>
  );
};

export default Welcome;
