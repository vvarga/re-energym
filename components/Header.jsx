import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image, Text } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { icons, images } from '../constants'; // Adjust the path to your icons
import ProfilePicture from '../lib/ProfilePicture';
import { useImageState } from '../context/ImageStateContext';

const Header = ({ onPressButton1, onPressButton2}) => {

  const { imageUri, setImageUri } = useImageState();

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={onPressButton1}>
          <Image source={icons.notifications} style={styles.notificationsIcon} />
          <Text style={styles.notifbuttonText}>Notifications</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={onPressButton2}>
          {imageUri ? (
            <Image source={{ uri: imageUri }} style={styles.defaultProfilePicture} />
          ) : (
            <Image source={images.account_image} style={styles.defaultProfilePicture} />
          )}
          <Text style={styles.buttonText}>Settings</Text>
         </TouchableOpacity>
  
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: '#fff', // Ensure the background color matches the header color
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center', // Align items to the center of the container
    paddingHorizontal: 20,
    // Add padding top to move buttons lower without expanding the header
    height: 40, // Fixed height for the header
    borderBottomWidth: 0,
    borderBottomColor: '#E8E8E8', // Add a border color for better separation
  },
  button: {
    width: 80, // Adjust the width of the buttons as needed
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff', // Example color, adjust as needed
    marginTop: 10, // Add margin top to lower the buttons without expanding the header
  },
  icon: {
    width: 20, // Adjust the width and height of the icon as needed
    height: 20,
    tintColor: 'black', // Example color, adjust as needed
  },
  notificationsIcon: {
    width: 40, // Adjust the width of the notifications icon
    height: 40, // Adjust the height of the notifications icon
  },
  buttonText: {
    fontSize: 10, // Adjust the font size as needed
    color: 'black', // Example color, adjust as needed
    marginTop: 5, // Add margin top to separate the text from the icon
  },
  profileButton: {
    alignItems: 'center',
  },
  notifbuttonText: {
    fontSize: 10, // Adjust the font size as needed
    color: 'black', // Example color, adjust as needed
    marginTop: 0, // Add margin top to separate the text from the icon
  },
  profilePicture: {
    width:  25, // Adjust the width and height of the profile picture as needed
    height: 25,
    borderRadius: 20, // Make it circular
  },
  defaultProfilePicture: {
    width: 40, // Adjust the width and height of the default profile picture as needed
    height: 40,
    borderRadius: 25, // Make it circular
    backgroundColor: '#E8E8E8', // Default background color
  },
});

export default Header;
