import profile from "../assets/images/profile.png";
import thumbnail from "../assets/images/thumbnail.png";
import cards from "../assets/images/cards.png";
import path from "../assets/images/path.png";
import logo from "../assets/images/mainlogo.png";
import treadmill from "../assets/images/treadmill.png";
import logoSmall from "../assets/images/logo-small.png";
import empty from "../assets/images/empty.png";
import lobstr from "../assets/images/lobstr2.png";
import account_image from "../assets/images/account_image.jpg";
import kfit from "../assets/images/Group 52.png";
import yog from "../assets/images/yog.png";
import full from "../assets/images/Group 53.png";
import zumba from "../assets/images/zumba.png";
import spinning from "../assets/images/Group 54.png";
import endurance from "../assets/images/Group 55.png";
import push from "../assets/images/Group 56.png";
import abs2 from "../assets/images/Group 61.png";
import treadmill2 from "../assets/images/Group 58.png";
import stairs from "../assets/images/Group 59.png";
import bike from "../assets/images/Group 60.png";
import hip from "../assets/images/Group.png";
import back from "../assets/images/Group 63.png";
import chest from "../assets/images/Group 64 (1).png";

import ch1 from "../assets/images/Group107.png"
import ch2 from "../assets/images/Group108.png"
import ch3 from "../assets/images/Group111.png"
import throphy from "../assets/images/throphy.png"
import trf1 from "../assets/images/trf1.png"
import trf2 from "../assets/images/trf2.png"
import trf3 from "../assets/images/trf3.png"
import swords from "../assets/images/crossed_swords.png"
import cable_m from "../assets/images/cable_m.png"
import row_m from "../assets/images/row_m.png"
import legpress_m from "../assets/images/legpress_m.png"
import legextension_m from "../assets/images/leg_extension_m.png"
import chest_press_m from "../assets/images/chest_press_m.png"
import squat_m from "../assets/images/squat_m.png"

export default {chest_press_m,squat_m, row_m,legpress_m,legextension_m, cable_m,profile, thumbnail, cards, path, logo, logoSmall, empty ,lobstr,kfit,zumba,full, yog, spinning, endurance, push, abs2, treadmill,treadmill2, stairs,bike,hip ,back,chest, swords, ch1, ch2, ch3, throphy, trf1, trf2, trf3, account_image};



