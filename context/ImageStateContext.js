// ImageStateContext.js
import React, { createContext, useContext, useState } from 'react';

const ImageStateContext = createContext();

export const useImageState = () => useContext(ImageStateContext);

export const ImageStateProvider = ({ children }) => {
  const [imageUri, setImageUri] = useState(null);

  return (
    <ImageStateContext.Provider value={{ imageUri, setImageUri }}>
      {children}
    </ImageStateContext.Provider>
  );
};
