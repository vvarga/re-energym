import React, { useState, useEffect } from 'react';
import { View, Button, Image, Platform, Alert,StyleSheet,Dimensions, TouchableOpacity ,Text} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { uploadProfilePicture } from './imageHandler';
import { images } from '../constants';
import { useImageState } from '../context/ImageStateContext';
import { useGlobalContext } from "../context/GlobalProvider";
import { currentAccount } from './appwrite';
const { width } = Dimensions.get('window');
const ProfilePictureUploader = ({ userId }) => {

  const { imageUri, setImageUri } = useImageState();
  const [uploadedFile, setUploadedFile] = useState(null);
 
  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          Alert.alert('Sorry, we need camera roll permissions to make this work!');
        } else {
          console.log('Media library permission granted');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    console.log('Image picker opened');
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });

      console.log('Image picker result:', result);

      if (!result.canceled && result.assets.length > 0) {
        const uri = result.assets[0].uri;
        console.log('Image selected:', uri);
        setImageUri(uri);
        handleUploadImage();
      } else {
        console.log('User canceled image picker');
      }
    } catch (error) {
      console.error('Error picking image:', error);
    }
  };

  const handleUploadImage = async () => {
    if (!imageUri) {
      console.log('No image selected');
      return;
    }

    console.log('Starting upload for:', imageUri);

    try {
      const file = await uploadProfilePicture(imageUri,userId);
      setUploadedFile(file);
      // setProfileImage(file); 
      console.log('File uploaded:', file);
    } catch (error) {
      console.error('Upload error:', error);
    }
  };

  return (
    <View>
      <TouchableOpacity style={styles.fullWidthButton} onPress={pickImage} >
        <Text>Change Your Profile Picture</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
fullWidthButton: {
  width: width - 40,
  height: 60,
  backgroundColor: '#E8E8E8',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: 20,
  shadowColor: 'rgba(0,0,0, .4)', // IOS
  shadowOffset: { height: 4, width: 1 }, // IOS
  shadowOpacity: 1, // IOS
  shadowRadius: 4, // IOS
  marginBottom:30
},
})
export default ProfilePictureUploader;
