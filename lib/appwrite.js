import 'react-native-url-polyfill/auto';
import { Databases, ID, Account, Client, Avatars, Storage,Query } from 'react-native-appwrite/src';
import { router } from 'expo-router';


export { storage };
export const config = {
    endpoint: 'https://cloud.appwrite.io/v1',
    platform: 'com.LekkerIndustries.Energym',
    proijectId: '6641ff42001077f81847',
    databaseId: '664202db003b3665f38c',
    userCollectionId: '66420318001fa6c8e6b8',
    videoCollectionId: '664202db003b3665f38c',
    sotrageId: '6642064600022f4c2791',
    machineCollectionId: '6660b8ed001f60f96b26'
}
// Init your React Native SDK

const client = new Client();
client
    .setEndpoint(config.endpoint) 
    .setProject(config.proijectId) 
    .setPlatform(config.platform) 
;

const account = new Account(client);
const storage = new Storage(client);
const avatars = new Avatars(client);
const databases = new Databases(client);

  
  const getActiveSessions = async () => {
    try {
        const sessions = await account.listSessions();
        console.log('Active sessions:', sessions);  // Debug logging
        return sessions;
    } catch (error) {
        console.log('Error fetching sessions:', error);
        return null;
}
};


  const deleteActiveSessions = async () => {
    try {
        const sessions = await getActiveSessions();
        if (sessions && sessions.sessions.length > 0) {
            for (const session of sessions.sessions) {
                console.log('Deleting session:', session.$id);  // Debug logging
                await account.deleteSession(session.$id);
            }
        }
    } catch (error) {
        console.log('Error deleting sessions:', error);
    }
};


// Update User Energy
export async function updateUserEnergy(userId, newEnergyValue) {
    try {

      // Update the user's energy field in the database
      await databases.updateDocument(config.databaseId, config.userCollectionId, '6661a5a6122e8f08e55a', {
        energy: newEnergyValue
      });
  
      console.log('User energy updated successfully');
    } catch (error) {
      console.error('Error updating user energy:', error.message);
      throw new Error('Failed to update user energy');
    }
  }
   
  
  export const getMachineDetails = (machineId) => {
    return new Promise((resolve, reject) => {
        console.log('Fetching machine details...');
        databases.listDocuments(
            config.databaseId,
            config.machineCollectionId,
            [
                Query.equal('machine_name', machineId)
            ]
        ).then(response => {
            console.log('Machine details response:', response);
            // Check if documents are found
            if (response.documents.length > 0) {
                // Extract energy_value from the first document
                const energyValue = response.documents[0].energy_value;
                console.log('Energy value:', energyValue);
                resolve({ energyValue });
            } else {
                const error = new Error('Machine not found');
                console.error(error.message);
                reject(error);
            }
        }).catch(error => {
            console.error('Error fetching machine details:', error);
            reject(error);
        });
    });
};



export const getUserEnergy = async (userId) => {

    let promise = databases.listDocuments(
        config.databaseId,
        config.userCollectionId,
        [
            Query.equal( 'accountId',userId)
        ]
    );
    
    promise.then(function (response) {
        console.log('mata',response);
    }, function (error) {
        console.log(error);
    });
};
  

  
  // Register user
  export async function createUser(email, password, username) {
    try {
      const newAccount = await account.create(ID.unique(), email, password, username);
  
      if (!newAccount) throw Error;
  
      const avatarUrl = avatars.getInitials(username);
  
      // Create a document for the user in the 'users' collection
      const newUser = await databases.createDocument(
        config.databaseId,
        config.userCollectionId,
        ID.unique(),
        {
          accountId: newAccount.$id,
          email: email,
          username: username,
          avatar: avatarUrl,
          energy: 0,
        }
      );
  
      // Sign in the user
      await signIn(email, password);
  
      return newUser;
    } catch (error) {
      console.error('Error creating user:', error.message);
      throw new Error('Failed to create user');
    }
  }
  
  // Sign In
  export async function signIn(email, password) {
    try {
        await deleteActiveSessions();
      const session = await account.createEmailPasswordSession(email, password);
  
      return session;
    } catch (error) {
      throw new Error(error);
    }
  }
  
  // Get Account
  export async function getAccount() {
    try {
      const currentAccount = await account.get();
  
      return currentAccount;
    } catch (error) {
      throw new Error(error);
    }
  }
  
  // Get Current User
  export async function getCurrentUser() {
    try {
        const currentAccount = await getAccount();
        console.log('Current account:', currentAccount);  // Log the account details

        if (!currentAccount) throw new Error('No current account found');

        const currentUser = await databases.listDocuments(
            config.databaseId,
            config.userCollectionId,
            [Query.equal("accountId", currentAccount.$id)]
        );

        console.log('User documents fetched:', currentUser.documents);  // Log the documents fetched

            

        return currentUser.documents[0];
    } catch (error) {
        console.error('Error fetching current user:', error.message);  // Log the specific error message
        return null;
    }
}

  
  // Sign Out
  export async function signOut() {
    try {
      const session = await account.deleteSession("current");
      router.dismissAll();
      return session;
    } catch (error) {
      throw new Error(error);
    }
  }
  
  // Upload File
  export async function uploadFile(file, type) {
    if (!file) return;
  
    const { mimeType, ...rest } = file;
    const asset = { type: mimeType, ...rest };
  
    try {
      const uploadedFile = await storage.createFile(
        config.storageId,
        ID.unique(),
        asset
      );
  
      const fileUrl = await getFilePreview(uploadedFile.$id, type);
      return fileUrl;
    } catch (error) {
      throw new Error(error);
    }
  }
  
  // Get File Preview
  export async function getFilePreview(fileId, type) {
    let fileUrl;
  
    try {
      if (type === "video") {
        fileUrl = storage.getFileView(config.storageId, fileId);
      } else if (type === "image") {
        fileUrl = storage.getFilePreview(
          config.storageId,
          fileId,
          2000,
          2000,
          "top",
          100
        );
      } else {
        throw new Error("Invalid file type");
      }
  
      if (!fileUrl) throw Error;
  
      return fileUrl;
    } catch (error) {
      throw new Error(error);
    }
  }
  
  // Create Video Post
  export async function createVideoPost(form) {
    try {
      const [thumbnailUrl, videoUrl] = await Promise.all([
        uploadFile(form.thumbnail, "image"),
        uploadFile(form.video, "video"),
      ]);
  
      const newPost = await databases.createDocument(
        config.databaseId,
        config.videoCollectionId,
        ID.unique(),
        {
          title: form.title,
          thumbnail: thumbnailUrl,
          video: videoUrl,
          prompt: form.prompt,
          creator: form.userId,
        }
      );
  
      return newPost;
    } catch (error) {
      throw new Error(error);
    }
  }
  
  // Get all video Posts
  export async function getAllPosts() {
    try {
      const posts = await databases.listDocuments(
        config.databaseId,
        config.videoCollectionId
      );
  
      return posts.documents;
    } catch (error) {
      throw new Error(error);
    }
  }
  
  // Get video posts created by user
  export async function getUserPosts(userId) {
    try {
      const posts = await databases.listDocuments(
        config.databaseId,
        config.videoCollectionId,
        [Query.equal("creator", userId)]
      );
  
      return posts.documents;
    } catch (error) {
      throw new Error(error);
    }
  }
  
  // Get video posts that matches search query
  export async function searchPosts(query) {
    try {
      const posts = await databases.listDocuments(
        config.databaseId,
        config.videoCollectionId,
        [Query.search("title", query)]
      );
  
      if (!posts) throw new Error("Something went wrong");
  
      return posts.documents;
    } catch (error) {
      throw new Error(error);
    }
  }

  
  
  // Get latest created video posts
  export async function getLatestPosts() {
    try {
      const posts = await databases.listDocuments(
        config.databaseId,
        config.videoCollectionId,
        [Query.orderDesc("$createdAt"), Query.limit(7)]
      );
  
      return posts.documents;
    } catch (error) {
      throw new Error(error);
    }
  }