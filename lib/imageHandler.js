import { storage } from './appwrite';
import { Alert } from 'react-native';

const BUCKET_ID = '665db5eb002607b8cb60'; // Replace with your bucket ID

export const uploadProfilePicture = async (uri, userId) => {
    console.log('Uploading image:', uri);
  
    try {
      // Check if the file already exists
      const existingFile = await storage.getFile(BUCKET_ID, `${userId}.png`);
      if (existingFile) {
        console.log('File already exists:', existingFile);
        return existingFile; // Return the existing file
      }
  
      const response = await fetch(uri);
      const fileBlob = await response.blob();
  
      console.log('File blob created:', fileBlob);
  
      const fileId = `${userId}.png`;
      console.log('Uploading with file ID:', fileId);
  
      const file = new File([fileBlob], fileId, { type: response.headers.get('content-type') });
  
      const result = await storage.createFile(BUCKET_ID, fileId, file);
      console.log('Upload result:', result);
  
      return result;
    } catch (error) {
      console.log('Upload result:', error);
      throw error;
    }
};
