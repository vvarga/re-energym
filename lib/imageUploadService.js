import { storage } from './appwrite'; // Make sure to import your Appwrite storage instance

const BUCKET_ID = '665db5eb002607b8cb60'; // Replace with your actual bucket ID

export const uploadProfilePicture = async (uri, userId) => {
  try {
    const response = await fetch(uri);
    const fileBlob = await response.blob();

    const fileId = `${userId}.png`;
    const result = await storage.createFile(BUCKET_ID, fileId, fileBlob);

    return result;
  } catch (error) {
    throw new Error(`Error uploading profile picture: ${error}`);
  }
};
